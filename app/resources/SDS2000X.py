import visa
from time import sleep

def connection_context(func):
    def inner(self, *args, **kwargs):
        self.__connect()
        func(self, *args, **kwargs)
        self.__close()
    return inner

class SDS2000X:
    # For more info on command syntax, look at SDS2000X programming guide
    # https://www.siglentamerica.com/wp-content/uploads/dlm_uploads/2017/10/Programming-Guide-1.pdf
    def __init__(self, name=None):
        self.__rm = visa.ResourceManager('@py')
        self.__name = name
        self.__sds = None
        self.common = self.__Common()
        self.header = self.__Header()
        self.acquire = self.__Acquire()
        self.autoset = self.__Autoset()
        self.channel = self.__Channel()
        self.cursor = self.__Cursor()
        self.digital = self.__Digital()
        self.display = self.__Display()
        self.history = self.__History()
        self.math = self.__Math()
        self.measure = self.__Measure()
        self.passfail = self.__PassFail()
        self.print = self.__Print()
        self.recall = self.__Recall()
        self.save = self.__Save()
        self.status = self.__Status()
        self.system = self.__System()
        self.timebase = self.__Timebase()
        self.trigger = self.__Trigger()
        self.waveform = self.__Waveform()
        self.wgen = self.__Wgen()

    def __connect(self):
        devices = self.__rm.list_resources()
        if self.__name in self.__rm.list_resources():
            self.__sds = self.__rm.open_resource(self.__name)
            return
        raise Exception("USB Oscilloscope device could not be located")

    @connection_context
    def query(self, cmd):
        return self.__sds.query(cmd)

    @connection_context
    def read(self):
        return self.__sds.read_raw()

    @connection_context
    def write(self, cmd):
        if type(cmd) is list or type(cmd) is tuple:
            time_wait = len(cmd)
            cmd = ';'.join(cmd__)

        self.__sds.write(new_cmd)
        sleep(1)

    def __close(self):
        self.__sds.close()

    def __del__(self):
        if self.__sds:
            self.__close()

    class __Common:

        @staticmethod
        def query_idn():
            # The *IDN? query identifies the instrument type
            # and software version. The response consists of
            # four different fields providing information on the
            # manufacturer, the scope model, the serial
            # number and the firmware revision.
            cmd = '*IDN?'
            return cmd

        @staticmethod
        def query_opc():
            # The *OPC? query places an ASCII "1" in the
            # output queue when all pending device operations
            # have completed. The interface hangs until this
            # query returns.
            cmd = '*OPC?'
            return cmd

        @staticmethod
        def cmd_opc():
            # The *OPC command sets the operation complete
            # bit in the Standard Event Status Register when
            # all pending device operations have finished.
            cmd = '*OPC'
            return cmd

        @staticmethod
        def cmd_rst():
            # The *RST command initiates a device reset.
            # This is the same as pressing [Default] on
            # the front panel.
            cmd = '*RST'
            return cmd

    class __Header:

        @staticmethod
        def cmd_chdr(mode='short'):
            # The COMM_HEADER command controls the
            # way the oscilloscope formats response to
            # queries
            if mode not in ('short', 'long', 'off'): raise ValueError("%s isn't a valid mode" % mode)
            cmd = 'chdr '+mode
            return cmd

        @staticmethod
        def query_chdr():
            # Returns the current header format
            cmd = 'chdr?'
            return cmd

    class __Acquire:

        @staticmethod
        def cmd_arm():
            # The ARM_ACQUISITION command starts a
            # new signal acquisition
            cmd = 'arm'
            return cmd

        @staticmethod
        def cmd_stop():
            # The STOP command stops the acquisition.
            # This is the same as pressing the Stop key on
            # the front panel
            cmd = 'stop'
            return cmd

        @staticmethod
        def cmd_acqw(mode='sampling', time=None):
            # The ACQUIRE_WAY command specifies the
            # acquisition mode
            if mode not in ('sampling', 'peak_detect', 'average', 'high_res'):
                raise ValueError("%s isn't a valid mode" % mode)
            max_exp = 15
            if time not in (1 << exp for exp in range(2, max_exp)):
                time = 16
            cmd = 'acqw '+mode
            if mode != 'sampling':
                cmd += ',{}'.format(time)
            return cmd

        @staticmethod
        def query_acqw():
            # The ACQUIRE_WAY? query returns the current
            # acquisition mode.
            cmd = 'acqw?'
            return cmd

        @staticmethod
        def cmd_avga(time=16):
            # The AVERAGE_ACQUIRE command selects
            # the average times of average acquisition
            max_exp = 15
            if time not in (1 << exp for exp in range(2, max_exp)):
                time = 16
            cmd = 'avga {}'.format(time)
            return cmd

        @staticmethod
        def query_avga():
            # The AVERAGE_ACQUIRE? query returns the
            # currently selected count value for average mode
            cmd = 'avga?'
            return cmd

        @staticmethod
        def cmd_msiz(size='14m'):
            # The MEMORY_SIZE command sets the
            # maximum depth of memory
            sizes = ('14k', '140k', '1.4m', '14m')
            if size not in sizes: raise ValueError("size %s isn't valid, it need to be [%s]" % (size, " ".join(sizes)))
            cmd = 'msiz {}'.format(size)
            return cmd

        @staticmethod
        def query_msiz():
            # The MEMORY_SIZE? query returns the
            # maximum depth of memory.
            cmd = 'msiz?'
            return cmd

        @staticmethod
        def query_sast():
            # The SAST? query returns the acquisition
            # status of the scope
            cmd = 'sast?'
            return cmd

        @staticmethod
        def query_sara():
            # The SARA? query returns the sample rate
            # of the scope.
            cmd = 'sara?'
            return cmd

        @staticmethod
        def query_sanu(channel='c1'):
            # The SANU? query returns the number of
            # data points that the hardware will acquire
            # from the input signal. The number of points
            # acquired is based on the horizontal scale
            # and memory/acquisition depth selections
            # and cannot be directly set.
            if channel not in ('c1', 'c2', 'c3', 'c4'): raise ValueError("Channel (%s) do not exist" % channel)
            cmd = 'sanu? '+channel
            return cmd

        @staticmethod
        def cmd_sxsa(state='off'):
            # The SINXX_SAMPLE command sets the
            # way of interpolation.
            if state not in ('on', 'off'): raise ValueError("Invalid state")
            cmd = 'sxsa '+state
            return cmd

        @staticmethod
        def query_sxsa():
            # The SINXX_SAMPLE query returns the
            # way of interpolation.
            cmd = 'sxsa?'
            return cmd

        @staticmethod
        def cmd_xyds(state='off'):
            # The XY_DISPLAY command enables or
            # disables the display of XY mode. XY mode
            # plots the voltage data of both channels with
            # respect to one-another. The standard
            # display mode plots voltage data vs. time.
            if state not in ('on', 'off'): raise ValueError("Invalid state")
            cmd = 'xyds '+state
            return cmd

        @staticmethod
        def query_xyds():
            # The XY_DISPLAY command enables or
            # disables the display of XY mode
            cmd = 'xyds?'
            return cmd

    class __Autoset:

        @staticmethod
        def cmd_aset():
            # The AUTO_SETUP command attempts to
            # identify the waveform type and automatically
            # adjusts controls to produce a usable display of
            # the input signal.
            cmd = 'aset'
            return cmd

    class __Channel:

        @staticmethod
        def cmd_attn(channel='c1', attenuation=1):
            # The ATTENUATION command specifies the
            # probe attenuation factor for the selected channel
            if channel not in ('c1', 'c2', 'c3', 'c4'): raise ValueError("Channel (%s) is unknown" % channel)
            if attenuation not in (0.1, 0.2, 0.5, 1, 2, 5, 10, 20, 50, 100, 200,
            500, 1000, 2000, 5000, 10000): raise ValueError("Invalid Attenuation")
            cmd = channel+':attn {}'.format(attenuation)
            return cmd

        @staticmethod
        def query_attn():
            # The ATTENUATION? query returns the current
            # probe attenuation factor for the selected channel.
            cmd = 'attn?'
            return cmd

        @staticmethod
        def cmd_bwl(channel='c1', mode='off'):
            # BANDWIDTH_LIMIT enables or disables the
            # bandwidth-limiting low-pass filter
            if channel not in ('c1', 'c2', 'c3', 'c4'): raise ValueError("Channel (%s) is unknown" % channel)
            if mode not in ('off', 'on'): raise ValueError("Mode (%s) is invalid" % mode)
            cmd = 'bwl '+channel+','+mode
            return cmd

        @staticmethod
        def query_bwl():
            # The BANDWIDTH_LIMIT? query returns
            # whether the bandwidth filters are on
            cmd = 'bwl?'
            return cmd

        @staticmethod
        def cmd_cpl(channel='c1', coupling='d50'):
            # The COUPLING command selects the coupling
            # mode of the specified input channel.
            if channel not in ('c1', 'c2', 'c3', 'c4'): raise ValueError("Channel (%s) invalid" % channel)
            if coupling not in ('a1m', 'a50', 'd1m', 'd50', 'gnd'): raise ValueError("Unknown coupling")
            cmd = channel+':cpl '+coupling
            return cmd

        @staticmethod
        def query_cpl():
            # The COUPLING? query returns the coupling
            # mode of the specified channel.
            cmd = 'cpl?'
            return cmd

        @staticmethod
        def cmd_ofst(channel='c1', offset='0v'):
            # The OFFSET command allows adjustment of the
            # vertical offset of the specified input channel. The
            # maximum ranges depend on the fixed sensitivity
            # setting.
            if channel not in ('c1', 'c2', 'c3', 'c4'): raise ValueError("Unknown Channel")
            cmd = channel+':ofst '+offset
            return cmd

        @staticmethod
        def query_ofst():
            # The OFFSET? query returns the offset value of
            # the specified channel.
            cmd = 'ofst?'
            return cmd

        @staticmethod
        def cmd_skew(channel='c1', skew='0ns'):
            # The SKEW command sets the channel-tochannel skew factor
            # for the specified channel. Each analog channel can be
            # adjusted + or -100 ns
            if channel not in ('c1', 'c2', 'c3', 'c4'): raise ValueError("Unknown channel")
            cmd = channel+':skew '+skew
            return cmd

        @staticmethod
        def query_skew(channel='c1'):
            # The SKEW? query returns the skew value of the specified trace.
            if channel not in ('c1', 'c2', 'c3', 'c4'): raise ValueError("Unknown channel")
            cmd = channel+':skew?'
            return cmd

        @staticmethod
        def cmd_tra(channel='c1', trace='off'):
            # The TRACE command turns the display of the
            # specified channel on or off.
            if channel not in ('c1', 'c2', 'c3', 'c4'): raise ValueError("Unknown Channel")
            if trace not in ('off', 'on'): raise ValueError("Unknown trace")
            cmd = channel+':tra '+trace
            return cmd

        @staticmethod
        def query_tra(channel='c1'):
            # The TRACE? query returns the current display
            # setting for the specified channel.
            if channel not in ('c1', 'c2', 'c3', 'c4'): raise ValueError()
            cmd = channel+':tra?'
            return cmd

        @staticmethod
        def cmd_unit(channel='c1', unit='V'):
            # The UNIT command sets the unit of the
            # specified trace
            if channel not in ('c1', 'c2', 'c3', 'c4'): raise ValueError()
            if unit not in ('V', 'A'): raise ValueError()
            cmd = channel+':unit '+unit
            return cmd

        @staticmethod
        def query_unit(channel='c1'):
            # The UNIT? query returns the unit of the
            # specified trace
            if channel not in ('c1', 'c2', 'c3', 'c4'): raise ValueError()
            cmd = channel+':unit?'
            return cmd

        @staticmethod
        def cmd_vdiv(channel='c1', vdiv='5V'):
            # The VOLT_DIV command sets the vertical
            # sensitivity in Volts/div.
            # If the probe attenuation is changed, the scale
            # value is multiplied by the probe's attenuation
            # factor.
            if channel not in ('c1', 'c2', 'c3', 'c4'): raise ValueError()
            cmd = channel+':vdiv '+vdiv
            return cmd

        @staticmethod
        def query_vdiv(channel='c1'):
            # The VOLT_DIV? query returns the vertical
            # sensitivity of the specified channel.
            if channel not in ('c1', 'c2', 'c3', 'c4'): raise ValueError()
            cmd = channel+':vdiv?'
            return cmd

        @staticmethod
        def cmd_invs(channel='c1', state='off'):
            # The INVERTSET command mathematically
            # inverts the specified traces or the math
            # waveform.
            if channel not in ('c1', 'c2', 'c3', 'c4', 'math'): raise ValueError()
            if state not in ('off', 'on'): raise ValueError()
            cmd = channel+':invs '+state
            return cmd

        @staticmethod
        def query_invs(channel='c1'):
            # The INVERTSET? query returns the current
            # state of the channel inversion
            if channel not in ('c1', 'c2', 'c3', 'c4'): raise ValueError()
            cmd = channel+':invs?'
            return cmd

    class __Cursor:

        @staticmethod
        def cmd_crms(mode='off'):
            # The CURSOR_MEASURE command specifies
            # the type of cursor or parameter measurement to
            # be displayed
            if mode not in ('off', 'on'): raise ValueError()
            cmd = 'crms '+mode
            return cmd

        @staticmethod
        def query_crms():
            # The CURSOR_MEASURE? query returns
            # which cursors or parameter measurements are
            # currently displayed.
            cmd = 'crms?'
            return cmd

        @staticmethod
        def cmd_crst(trace='c1', cursors='', positions=''):
            # The CURSOR_SET command allows the user to
            # position any one of the four independent cursors
            # at a given screen location
            if trace not in ('c1', 'c2', 'c3', 'c4'): raise ValueError()
            cmd = trace+':crms '
            cur_amt = len(cursors)
            for idx in range(cur_amt):
                if cursors[idx] in ('vref', 'vdif', 'tref', 'tdif', 'hrdf', 'hdif',):
                    cmd += cursors[idx]+','+positions[idx]+','
            cmd = cmd[:-1]
            return cmd

        @staticmethod
        def query_crst(trace='c1', cursors=''):
            # The CURSOR_SET? query returns the current
            # position of the cursor(s)
            if trace not in ('c1', 'c2', 'c3', 'c4'): raise ValueError()
            cmd = trace+':crms? '
            for cursor in cursors:
                if cursor in ('vref', 'vdif', 'tref', 'tdif', 'href', 'hdif',):
                    cmd += cursor+','
            cmd = cmd[:-1]
            return cmd

        @staticmethod
        def cmd_crty(type):
            # The CURSOR_TYPE command specifies the
            # type of cursor to be displayed when the cursor
            # mode is manual.
            cmd = 'crty '
            if type in ('x', 'y', 'x-y'):
                cmd += type
            return cmd

        @staticmethod
        def query_crty():
            # The CURSOR_TYPE query returns the current
            # type of cursor.
            cmd = 'crty?'
            return cmd

        @staticmethod
        def query_crva(trace='c1', mode='hrel'):
            # The CURSOR_VALUE? query returns the
            # values measured by the specified cursors for a
            # given trace.
            if trace not in ('c1', 'c2', 'c3', 'c4'): raise ValueError()
            if mode not in ('hrel', 'vrel'): raise ValueError()
            cmd = trace+':crva? '+mode
            return cmd

    class __Digital:

        @staticmethod
        def cmd_dgch(digital='d0', state='off'):
            if digital not in ('d0', 'd1', 'd2', 'd3', 'd4', 'd5', 'd6', 'd7',
                               'd8', 'd9', 'd10', 'd11', 'd12', 'd13', 'd14', 'd15'):
                raise ValueError()
            if state not in ('off', 'on'): raise ValueError()
            cmd = digital+':dgch '+state
            return cmd

        @staticmethod
        def query_dgch(digital='d0'):
            if digital not in ('d0', 'd1', 'd2', 'd3', 'd4', 'd5', 'd6', 'd7',
                               'd8', 'd9', 'd10', 'd11', 'd12', 'd13', 'd14', 'd15'):
                raise ValueError()
            cmd = digital+':dgch?'
            return cmd

        @staticmethod
        def cmd_dgst(state='off'):
            if state not in ('off', 'on'): raise ValueError()
            cmd = 'dgst '+state
            return cmd

        @staticmethod
        def query_dgst():
            cmd = 'dgst?'
            return cmd

        @staticmethod
        def cmd_dgth(group='c1', type='ttl', level='0v'):
            if group not in ('c1', 'c2'): raise ValueError()
            if type not in ('ttl', 'cmos', 'cmos3.3', 'cmos2.5', 'custom'): raise ValueError()
            cmd = group + ':dgth ' + type
            if type == 'custom': cmd += ','+level
            return cmd

        @staticmethod
        def query_dgth(group='c1'):
            if group not in ('c1', 'c2'): raise ValueError()
            cmd = group + ':dgth?'
            return cmd

    class __Display:

        @staticmethod
        def cmd_dtjn(state='off'):
            if state not in ('off', 'on'): raise ValueError()
            cmd = 'dtjn '+state
            return cmd

        @staticmethod
        def query_dtjn():
            cmd = 'dtjn?'
            return cmd

        @staticmethod
        def cmd_grds(type='full'):
            if type not in ('full', 'half', 'off'): raise ValueError()
            cmd = 'grds '+type
            return cmd

        @staticmethod
        def query_grds():
            cmd = 'grds?'
            return cmd

        @staticmethod
        def cmd_ints(grid=False, grid_val=50, trace=False, trace_val=50):
            cmd = 'ints '
            if grid:
                if grid_val not in range(101): grid_val = 50
                cmd += 'grid,{},'.format(grid_val)
            if trace:
                if trace_val not in range(101): trace_val = 50
                cmd += 'trace,{},'.format(trace_val)
            cmd = cmd[:-1]
            return cmd

        @staticmethod
        def query_ints():
            cmd = 'ints?'
            return cmd

        @staticmethod
        def cmd_menu(state='on'):
            if state not in ('off', 'on'): raise ValueError()
            cmd = 'menu '+state
            return cmd

        @staticmethod
        def query_menu():
            cmd = 'menu?'
            return cmd

        @staticmethod
        def cmd_pesu(time=1):
            if time not in ('infinite', 1, 5, 10, 30): raise ValueError()
            cmd = 'pesu {}'.format(time)
            return cmd

        @staticmethod
        def query_pesu():
            cmd = 'pesu?'
            return cmd

    class __History:

        @staticmethod
        def cmd_fram(frame_num=0):
            cmd = 'fram {}'.format(frame_num)
            return cmd

        @staticmethod
        def query_ftim():
            cmd = 'ftim?'
            return cmd

    class __Math:

        @staticmethod
        def cmd_def(ch1='c1', func='+', ch2='c1'):
            if ch1 not in ('c1', 'c2', 'c3', 'c4'): raise ValueError()
            if ch2 not in ('c1', 'c2', 'c3', 'c4'): raise ValueError()
            if func not in ('+', '-', '*', '/', 'fft', 'intg', 'diff', 'sqrt'): raise ValueError()
            if func in ('fft', 'intg', 'diff', 'sqrt'):
                cmd = "def eqn,'"+func+ch1+"'"
            else:
                cmd = "def eqn,'"+ch1+func+ch2+"'"
            return cmd

        @staticmethod
        def query_def():
            cmd = 'def?'
            return cmd

        @staticmethod
        def cmd_invs(state='off'):
            if state not in ('off', 'on'): raise ValueError()
            cmd = 'math:invs '+state
            return cmd

        @staticmethod
        def query_invs():
            cmd = 'math:invs?'
            return cmd

        @staticmethod
        def cmd_mtvd(scale='1v'):
            cmd = 'mtvd '+scale
            return cmd

        @staticmethod
        def query_mtvd():
            cmd = 'mtvd?'
            return cmd

        @staticmethod
        def cmd_mtvp(point=0):
            cmd = 'mtvp {}'.format(point)
            return cmd

        @staticmethod
        def query_mtvp():
            cmd = 'mtvp?'
            return cmd

        @staticmethod
        def cmd_fftf(state='off'):
            if state not in ('off', 'on', 'exclu'): raise ValueError()
            cmd = 'fftf '+ state
            return cmd

        @staticmethod
        def query_fftf():
            cmd = 'fftf?'
            return cmd

        @staticmethod
        def cmd_ffts(scale=1):
            cmd = 'ffts {}'.format(scale)
            return cmd

        @staticmethod
        def query_ffts():
            cmd = 'ffts?'
            return cmd

        @staticmethod
        def cmd_fftw(window="hann"):
            if window not in ('rect', 'blac', 'hann', 'hamm', 'flattop'): raise ValueError()
            cmd = 'fftw '+window
            return cmd

        @staticmethod
        def query_fftw():
            cmd = 'fftw?'
            return cmd

    class __Measure:

        @staticmethod
        def query_cymt():
            cmd = 'cymt?'
            return cmd

        @staticmethod
        def cmd_mead(source='c1-c2', type='frr'):
            if source not in ('c1-c2', 'c1-c3', 'c1-c4', 'c2-c3', 'c2-c4', 'c3-c4'):
                raise ValueError()
            if type not in ('pha', 'frr', 'frf', 'ffr', 'fff', 'lrr', 'lrf', 'lfr', 'lff', 'skew'):
                raise ValueError()
            cmd = 'mead ' + type + ',' + source
            return cmd

        @staticmethod
        def query_mead(source='c1-c2', type='frr'):
            if source not in ('c1-c2', 'c1-c3', 'c1-c4', 'c2-c3', 'c2-c4', 'c3-c4'):
                raise ValueError()
            if type not in ('pha', 'frr', 'frf', 'ffr', 'fff', 'lrr', 'lrf', 'lfr', 'lff', 'skew'):
                raise ValueError()
            cmd = source+':mead? '+type
            return cmd

        @staticmethod
        def cmd_pacu(source='c1', param='pkpk'):
            if source not in ('c1', 'c2', 'c3', 'c4'): raise ValueError()
            if param not in ('pkpk', 'max', 'min', 'ampl', 'top', 'base', 'cmean', 'mean', 'rms',
                             'crms', 'ovsn', 'fpre', 'ovsp', 'rpre', 'per', 'freq', 'pwid', 'nwid',
                             'rise', 'fall', 'wid', 'duty', 'nduty', 'all'):
                raise ValueError()
            cmd = 'pacu '+param+','+source
            return cmd

        @staticmethod
        def query_pava(source='c1', param='pkpk', cust=None, stat=None):
            if source not in ('c1', 'c2', 'c3', 'c4'): raise ValueError()
            if param not in ('pkpk', 'max', 'min', 'ampl', 'top', 'base', 'cmean', 'mean', 'rms',
                             'crms', 'ovsn', 'fpre', 'ovsp', 'rpre', 'per', 'freq', 'pwid', 'nwid',
                             'rise', 'fall', 'wid', 'duty', 'nduty', 'all'):
                raise ValueError()
            if cust:
                cmd = 'pava? cust{}'.format(cust)
            elif stat:
                cmd = 'pava? stat{}'.format(stat)
            else:
                cmd = source+':pava? '+param
            return cmd

    class __PassFail:

        @staticmethod
        def cmd_pacl():
            cmd = 'pacl'
            return cmd

        @staticmethod
        def cmd_pfbf(state='off'):
            if state not in ('off', 'on'): raise ValueError()
            cmd = 'pfbf '+state
            return cmd

        @staticmethod
        def query_pfbf():
            cmd = 'pfbf?'
            return cmd

        @staticmethod
        def cmd_pfcm():
            cmd = 'pfcm'
            return cmd

        @staticmethod
        def query_pfdd():
            cmd = 'pfdd?'
            return cmd

        @staticmethod
        def cmd_pfds(state='off'):
            if state not in ('off', 'on'): raise ValueError()
            cmd = 'pfds '+state
            return cmd

        @staticmethod
        def query_pfds():
            cmd = 'pfds?'
            return cmd

        @staticmethod
        def cmd_pfst(xdiv, ydiv):
            cmd = 'pfst xmask,{},ymask,{}'.format(xdiv,ydiv)
            return cmd

        @staticmethod
        def query_pfst():
            cmd = 'pfst?'
            return cmd

    class __Print:

        @staticmethod
        def query_scdp():
            cmd = 'scdp'
            return cmd

    class __Recall:

        @staticmethod
        def cmd_rcl(setup_num):
            cmd = '*rcl {}'.format(setup_num)
            return cmd

        @staticmethod
        def cmd_rcpn(filename='test.xml'):
            cmd = "rcpn disk,udsk,file,'"+filename+"'"
            return cmd

    class __Save:

        @staticmethod
        def cmd_sav(setup_num):
            cmd = '*sav {}'.format(setup_num)
            return cmd

        @staticmethod
        def cmd_pnsu(binary_data):
            cmd = 'pnsu {}'.format(binary_data)
            return cmd

        @staticmethod
        def query_pnsu():
            cmd = 'pnsu?'
            return cmd

        @staticmethod
        def cmd_stpn(filename='test.xml'):
            cmd = "stpn disk,udsk,file,'"+filename+"'"
            return cmd

    class __Status:

        @staticmethod
        def query_inr():
            cmd = 'inr?'
            return cmd

    class __System:

        @staticmethod
        def query_cal():
            cmd = '*cmd?'
            return cmd

        @staticmethod
        def cmd_buzz(state='off'):
            if state not in ('off', 'on'): raise ValueError()
            cmd = 'buzz '+state
            return cmd

        @staticmethod
        def query_buzz():
            cmd = 'buzz?'
            return cmd

        @staticmethod
        def cmd_conet(ip=""):
            reformatted_ip = ip.replace(".", ",")
            cmd = 'conet '+reformatted_ip
            return cmd

        @staticmethod
        def query_conet():
            cmd = 'conet?'
            return cmd

        @staticmethod
        def cmd_scsv(time="off"):
            if time not in ('off', '1min', '5min', '10min', '30min', '60min'): raise ValueError()
            cmd = 'scsv '+time
            return cmd

        @staticmethod
        def query_scsv():
            cmd = 'scsv?'
            return cmd

    class __Timebase:

        @staticmethod
        def cmd_tdiv(value="200ms"):
            if value not in ('1ns', '2ns', '5ns', '10ns', '20ns', '50ns',
                             '100ns', '200ns', '500ns', '1us', '2us', '5us',
                             '10us', '20us', '50us', '100us', '200us', '500us',
                             '1ms', '2ms', '5ms', '10ms', '20ms', '50ms', '100ms',
                             '200ms', '500ms', '1s', '2s', '5s', '10s', '20s', '50s', '100s'):
                raise ValueError()
            cmd = 'tdiv '+value
            return cmd

        @staticmethod
        def query_tdiv():
            cmd = 'tdiv?'
            return cmd

        @staticmethod
        def cmd_trdl(time_delay):
            cmd = 'trdl '+time_delay
            return cmd

        @staticmethod
        def query_trdl():
            cmd = 'trdl?'
            return cmd

        @staticmethod
        def cmd_hmag(value="200ms"):
            if value not in ('1ns', '2ns', '5ns', '10ns', '20ns', '50ns',
                             '100ns', '200ns', '500ns', '1us', '2us', '5us',
                             '10us', '20us', '50us', '100us', '200us', '500us',
                             '1ms', '2ms', '5ms', '10ms', '20ms'):
                raise ValueError()
            cmd = 'hmag '+value
            return cmd

        @staticmethod
        def query_hmag():
            cmd = 'hmag?'
            return cmd

        @staticmethod
        def cmd_hpos(position):
            cmd = 'hpos '+position
            return cmd

        @staticmethod
        def query_hpos():
            cmd = 'hpos?'
            return cmd

    class __Trigger:

        @staticmethod
        def cmd_set50():
            cmd = 'set50'
            return cmd

        @staticmethod
        def cmd_trcp(source='c1', coupling='dc'):
            if source not in ('c1', 'c2', 'c3', 'c4', 'ex', 'ex5'): raise ValueError()
            if coupling not in ('ac', 'dc', 'hfrej', 'lfrej'): raise ValueError()
            cmd = source + ':trcp ' + coupling
            return cmd

        @staticmethod
        def query_trcp(source='c1'):
            if source not in ('c1', 'c2', 'c3', 'c4', 'ex', 'ex5'): raise ValueError()
            cmd = source+':trcp?'
            return cmd

        @staticmethod
        def cmd_trlv(source='c1', level=''):
            if source not in ('c1', 'c2', 'c3', 'c4', 'ex', 'ex5'): raise ValueError()
            cmd = source+':trlv '+level
            return cmd

        @staticmethod
        def query_trlv(source='c1'):
            if source not in ('c1', 'c2', 'c3', 'c4', 'ex', 'ex5'): raise ValueError()
            cmd = source+':trlv?'
            return cmd

        @staticmethod
        def cmd_trlv2(source='c1', level=''):
            if source not in ('c1', 'c2', 'c3', 'c4', 'ex', 'ex5'): raise ValueError()
            cmd = source+':trlv2 '+level
            return cmd

        @staticmethod
        def query_trlv2(source='c1'):
            if source not in ('c1', 'c2', 'c3', 'c4'): raise ValueError()
            cmd = source+':trlv2?'
            return cmd

        @staticmethod
        def cmd_trmd(mode="auto"):
            if mode not in ('auto', 'norm', 'single', 'stop'): raise ValueError()
            cmd = 'trmd '+mode
            return cmd

        @staticmethod
        def query_trmd():
            cmd = 'trmd?'
            return cmd

        @staticmethod
        def cmd_trpa(sources, statuses, condition):
            min_len = min(len(l) for l in (sources, statuses))
            cmd = 'trpa '
            for idx in range(min_len):
                cmd += sources[idx]+','+statuses[idx]+','
            cmd += 'state,'+condition
            return cmd

        @staticmethod
        def query_trpa():
            cmd = 'trpa?'
            return cmd

        @staticmethod
        def cmd_trse(trig_type='edge',source='c1',hold_type='',value1='',value2=''):
            if trig_type not in ('edge', 'slew', 'glit', 'intv', 'runt', 'drop'): raise ValueError()
            if source not in ('c1', 'c2', 'c3', 'c4', 'line', 'ex', 'ex5'): raise ValueError()
            cmd = 'trse '+trig_type+',sr,'+source+',ht,'+hold_type
            if value1:
                cmd += ',hv,'+value1
            if value2:
                cmd += ',hv2,'+value2
            return cmd

        @staticmethod
        def query_trse():
            cmd = 'trse?'
            return cmd

        @staticmethod
        def cmd_trsl(source='c1', slope='pos'):
            if source not in ('c1', 'c2', 'c3', 'c4', 'ex', 'ex5'): raise ValueError()
            if slope not in ('neg', 'pos', 'window'): raise ValueError()
            cmd = source+':trsl '+slope
            return cmd

        @staticmethod
        def query_trsl(source='c1'):
            if source not in ('c1', 'c2', 'c3', 'c4', 'ex', 'ex5'): raise ValueError()
            cmd = source+':trsl?'
            return cmd

        @staticmethod
        def cmd_trwi(value):
            cmd = 'trwi {}'.format(value)
            return cmd

        @staticmethod
        def query_trwi():
            cmd = 'trwi?'
            return cmd

    class __Waveform:

        @staticmethod
        def query_wf(trace='c1'):
            if trace not in ('c1', 'c2', 'c3', 'c4', 'math', 'd0', 'd1', 'd2', 'd3', 'd4',
                             'd5', 'd6', 'd7', 'd8', 'd9', 'd10', 'd11', 'd12', 'd13',
                             'd14', 'd15'):
                raise ValueError()
            cmd = trace+':wf? dat2'
            return cmd

        @staticmethod
        def cmd_wfsu(sparsing, num_points, first_point):
            cmd = 'wfsu '
            if sparsing:
                cmd += 'sp,'+sparsing+','
            if num_points:
                cmd += 'np,'+num_points+','
            if first_point:
                cmd += 'fp,'+first_point+','
            cmd = cmd[:-1]
            return cmd

        @staticmethod
        def query_wfsu():
            cmd = 'wfsu?'
            return cmd

    class __Wgen:

        @staticmethod
        def cmd_arwv(index=1):
            if index not in range(10): raise ValueError()
            cmd = 'arwv index,{}'.format(index)
            return cmd

        @staticmethod
        def query_prod(parameter):
            if parameter not in ('model', 'band'): raise ValueError()
            cmd = 'prod? '+parameter
            return cmd

        @staticmethod
        def query_stl(type):
            if type not in ('debug', 'release'): raise ValueError()
            cmd = 'stl? '+type
            return cmd

        @staticmethod
        def cmd_wgen(parameter, value):
            cmd = 'wgen '+parameter+',{}'.format(value)
            return cmd

        @staticmethod
        def query_wgen(parameter):
            cmd = 'wgen? '+parameter
            return cmd

        @staticmethod
        def query_wvpr(index):
            cmd = 'wvpr? {}'.format(index)
            return cmd

    # @property
    # def common(self):
    #     return self.__common
    #
    # @property
    # def header(self):
    #     return self.__header
    #
    # @property
    # def acquire(self):
    #     return self.__acquire
    #
    # @property
    # def autoset(self):
    #     return self.__autoset
    #
    # @property
    # def channel(self):
    #     return self.__channel
    #
    # @property
    # def cursor(self):
    #     return self.__cursor
    #
    # @property
    # def digital(self):
    #     return self.__digital
    #
    # @property
    # def display(self):
    #     return self.__display
    #
    # @property
    # def history(self):
    #     return self.__history
    #
    # @property
    # def math(self):
    #     return self.__math
    #
    # @property
    # def measure(self):
    #     return self.__measure
    #
    # @property
    # def passfail(self):
    #     return self.__passfail
    #
    # @property
    # def print(self):
    #     return self.__print
    #
    # @property
    # def recall(self):
    #     return self.__recall
    #
    # @property
    # def save(self):
    #     return self.__save
    #
    # @property
    # def status(self):
    #     return self.__status
    #
    # @property
    # def system(self):
    #     return self.__system
    #
    # @property
    # def timebase(self):
    #     return self.__timebase
    #
    # @property
    # def trigger(self):
    #     return self.__trigger
    #
    # @property
    # def waveform(self):
    #     return self.__waveform
    #
    # @property
    # def wgen(self):
    #     return self.__wgen

if __name__ == "__main__":
    import inspect

    a = SDS2000X('asd')

    cmds = []
    def method_checker(base):
        for thing in dir(base):
            if callable(getattr(base,thing)) and thing[0] != "_":
                cmds.append(thing)
        return cmds

    print(method_checker(a))
