import logging
import random
import time


class TestResource:
    def __init__(self, host, port):
        self.host = host
        self.port = port
        self.connected = False

    def __connect(self):
        logging.info(f"Connecting to test resource located at {self.host}:{self.port}")
        self.connected = True

    def __disconnect(self):
        logging.info(f"Disconnecting from test resource located at {self.host}:{self.port}")
        self.connected = False

    def __enter__(self):
        self.__connect()
        return self

    def __exit__(self, exception_type, exception_value, traceback):
        self.__disconnect()
        if exception_type:
            logging.warning(f"{exception_type = }\n")
            logging.warning(f"{exception_value = }\n")
            logging.warning(f"{traceback = }\n")

    def query(self, cmd):
        logging.info(f"Querying test resource located at {self.host}:{self.port} with the following cmd: {cmd}")
        time.sleep(random.randint(0, 5))
        return "GOOD"

    def read(self, cmd):
        logging.info(f"Reading from test resource located at {self.host}:{self.port} with the following cmd: {cmd}")
        time.sleep(random.randint(0, 5))
        return "GOOD"

    def write(self, cmd):
        logging.info(f"Writing to test resource located at {self.host}:{self.port} with the following cmd: {cmd}")
        time.sleep(random.randint(0, 5))


if __name__ == "__main__":
    with TestResource("localhost", 6300) as t:
        t.query("*IDN?")
    logging.info("DONE")
