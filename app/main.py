from fastapi import FastAPI, BackgroundTasks
from typing import Dict, Union
import logging
import asyncio
import os
from app.tasks.tasks import task_run
from app.utils.utils import (
    JSON_400,
    Resource,
    Task,
    SerializedTask,
    RedisJsonClient,
    gen_uuid4,
    chunks,
    import_resource,
    ErrorContainer,
    ConfigReader,
)


################################################################################
#                        Application Business Logic                            #
################################################################################
def __task_data_error(data: Task):
    # Check if resources listed are valid & have valid commands
    for dev_info in data.resources:
        dev = next(iter(dev_info.keys()))  # Getting device name
        cmds = next(iter(dev_info.values()))  # Getting device command(s)
        if dev not in app.resources:
            return JSON_400(content={"error": f"Unknown device '{dev}'"})
        for cmd_info in cmds:
            cmd = next(iter(cmd_info.keys()))  # Getting device command
            if cmd not in app.resources[dev].commands:
                msg = {"error": f"Unknown command '{cmd}' for device '{dev}'"}
                return JSON_400(content=msg)
    return None


def add_device(resource_class_name: str, name: str) -> None:
    """ Registers a new Measurement Resource instance with the application """
    if name in app.resources:
        logging.warning(f"Resource with name '{name}' already exists")
        return None
    else:
        logging.info(f"Adding {name} to resources")
        resource_class = import_resource(resource_class_name)
        if isinstance(resource_class, ErrorContainer):
            logging.error(
                f"Caught '{resource_class.error_type.__name__}' when "
                f"trying to add resource: {resource_class.error_msg}"
            )
            return None
        device_methods = [
            method
            for method in dir(resource_class)
            if callable(getattr(resource_class, method)) and method[0] != "_"
        ]
        app.redis_client.add_resource(resource_name=name)
        app.resources[name] = Resource(
            name=name, object=resource_class, commands=device_methods
        )

        logging.info(
            f"Found the following commands for {name} : "
            f"{app.resources[name].commands}"
        )
        return None


async def resource_checker():
    while True:
        unutilized_resources = app.redis_client.get_unintilized_resources()
        task_list_length = app.redis_client.task_list_length()

        if (task_list_length > 0) and unutilized_resources:
            for resource_name in unutilized_resources:
                # Return task_id of first user of resource, else None
                task_id = await get_resource_user(task_list_length, resource_name)

                if task_id:  # If we get a uuid string then we run the task
                    logging.info(
                        f"Running task {task_id} with the {resource_name} resource"
                    )
                    # Check if it is a chained task first
                    task_is_chained = app.redis_client.task_is_chained(task_id)
                    app.redis_client.lock_resource(
                        resource_name, task_id, task_is_chained
                    )
                    task_run.delay(task_id, resource_name, task_is_chained)

        await asyncio.sleep(5)


async def get_resource_user(task_list_length: int, resource_name: str) -> Union[str, None]:
    """
    Checks the Redis backend for the earliest task that requires
    access to the resource specified by the `resource_name` arg.
    """
    # Look at a specific amt of task resources at a time, not all
    for task_idx_chunk in chunks(task_list_length, 5):
        task_id_chunk = app.redis_client.get_task_range(*task_idx_chunk)
        task_info_list = app.redis_client.get_tasks(task_id_chunk)
        for task_id, task_info in zip(task_id_chunk, task_info_list):
            chain_conflict = await check_for_chaining_conflict(
                task_id=task_id, task_info=task_info
            )
            if chain_conflict:  # If the task conflicts with a running chained
                continue  # task, then skip over it
            is_first = await resource_first_in_task(
                task_info=task_info, resource_name=resource_name
            )
            if is_first:
                return task_id
    return None


async def check_for_chaining_conflict(task_id: str, task_info: Dict) -> bool:
    # Confirm the task is a chained task
    if not task_info.get("chain"):
        return False

    resources = [next(iter(name.keys())) for name in task_info.get("resources")]

    for act in app.redis_client.get_active_chained_tasks():
        if act == task_id:  # Confirm we aren't checking the same task
            return False

        act_resources = app.redis_client.get_task_resource_names(act)
        # Check for similarities b/ween active chained task &
        if set(resources) & set(act_resources):
            return True

    return False


async def resource_first_in_task(task_info: Dict, resource_name: str) -> Union[bool, int]:
    " Not an async task by any means but is made async to free up event loop "
    # Do not check tasks that are running or complete
    if task_info.get("busy"):
        return False

    resources = [next(iter(name.keys())) for name in task_info.get("resources")]
    return resource_name == resources[0]


################################################################################
#                               Application                                    #
################################################################################
app = FastAPI()
loop = asyncio.get_running_loop()  # Grab the running uvloop

# __ Application Setup _______________________________________________________
app.redis_client = RedisJsonClient("rejson", 6379)
app.name: str = "RemIS"  # Name of server
app.resources: Dict[str, Resource] = {}  # Dict of all connected resourcess
app.resource_file = os.getenv('REMIS_RESOURCE_CFG', 'app/resources/test_resources.yaml')

# __ Application Endpoints ___________________________________________________
@app.on_event("startup")
async def startup_event():
    # Adding Resources
    cfg = ConfigReader(app.resource_file)
    for resource_name in cfg.get_resources():
        resource_info = cfg.get_resource_info(resource_name)
        add_device(resource_info.class_name, resource_name)
    # Kicking off task that monitors the measurement queue
    loop.create_task(resource_checker())


@app.get("/")
async def root():
    return {"server": app.name}


@app.get("/list")
async def device_list():
    device_list = [dev for dev in app.resources]
    reply = {"device_list": device_list}
    return reply if device_list else {"msg": "No resources found"}


@app.get("/device/{dev_name}")
async def device_info(dev_name: str):
    if dev_name in app.resources:
        dev_copy = app.resources[dev_name]._asdict().copy()
        dev_copy.pop("object")
        data = dev_copy
    else:
        data = JSON_400(content={"error": f"Unknown device '{dev_name}'"})
    return data


@app.post("/run_task")
async def run_task(task: Task, background_tasks: BackgroundTasks):
    # Check for device & method declaration errors
    err = __task_data_error(task)
    if err:
        return err

    task_id = str(gen_uuid4())
    serialized_task = SerializedTask(task_id=task_id, task=task)
    _ = app.redis_client.push_task(serialized_task=serialized_task)

    return {"task_id": task_id}


@app.get("/get_task_result/{task_id}")
async def get_task_result(task_id: str):
    task_result = app.redis_client.get_task_result(task_id)
    status = "Done" if task_result else "Pending"
    return {"status": status, "result": task_result}
