import requests


class RemisClient:
    ''' Base client class that creates tasks '''
    def __init__(self, host, port, test_client=False):
        self.host = host
        self.port = port
        self.__url = f"http://{self.host}:{self.port}"
        self._test = test_client  # True if client is to be used for testing

    def create_task(self, chain=False):
        return self.TaskCreator(root=self, chain=chain)

    def index(self):
        reply = requests.get(self.__url + "/")
        return reply.json() if not self._test else reply

    def list_devices(self):
        reply = requests.get(self.__url + "/list")
        return reply.json()['device_list'] if not self._test else reply

    def device_info(self, device_name):
        reply = requests.get(self.__url + f"/device/{device_name}")
        return reply.json() if not self._test else reply

    class TaskCreator:
        ''' Task creation class, creates resources that add to the task '''
        def __init__(self, root=None, chain=False):
            self.__chain = chain
            self.__root = root
            self.__host = None if root is None else root.host
            self.__port = None if root is None else root.port
            self.__task_id = None
            self.resource_list = []

        @property
        def chain(self):
            return self.__chain

        @chain.setter
        def chain(self, value):
            if type(value) is not bool:
                raise ValueError("Task chain value must be a boolean")
            self.__chain = value

        def add_resource(self, resource_name):
            resource = self.ResourceCreator(resource_name)
            self.resource_list.append(resource)
            return resource

        def create_task(self):
            resources = [{r.name: r.func_list} for r in self.resource_list]
            task = {"chain": self.chain, "resources": resources}
            return task

        def send_task(self):
            task = self.create_task()
            reply = requests.post(f"http://{self.__host}:{self.__port}/run_task", json=task)
            task_id = reply.json().get('task_id', None)
            self.__task_id = task_id
            return task_id

        def get_result(self, task_id=None):
            if (self.__task_id is None) and (task_id is None):
                return None
            if task_id is None:
                task_id = self.__task_id
            reply = requests.get(f"http://{self.__host}:{self.__port}/get_task_result/{task_id}")
            return reply.json()

        class ResourceCreator:
            def __init__(self, resource_name):
                self.name = resource_name
                self.func_list = []

            def add_func(self, func_name, *args):
                func = {func_name: args}
                self.func_list.append(func)
                return self  # Return self so we can chain these add_func calls


if __name__ == "__main__":
    client = RemisClient('127.0.0.1', 8000)

    task1 = client.create_task()
    task1.chain = True  # Should default to False
    r1 = task1.add_resource('TestResource1')
    r1.add_func('query', '*IDN?')
    r1.add_func('write', 'arg1')
    r2 = task1.add_resource('TestResource2')
    r2.add_func('read', 'arg1')

    print(task1.create_task())

    # ----------- or ------------

    task2 = client.create_task(chain=True)
    task2.add_resource('TestResource2').add_func('query', '*IDN?').add_func('write', 'arg1')
    task2.add_resource('TestResource1').add_func('read', 'arg1')

    print(task2.create_task())

    # print(task1.send_task())

    # print(task1.get_result())
