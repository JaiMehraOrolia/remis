# ****************************************************************************#
#                         APPLICATION HELPERS/UTILS                           #
# ****************************************************************************#
from functools import partial
from fastapi.responses import JSONResponse
from fastapi import status
from pydantic import BaseModel
from typing import List, Dict, Type, Tuple, NamedTuple, Callable, Any, Union
from importlib import import_module
from collections import namedtuple
import uuid
import rejson
import yaml

# __ Responses & Data Types _________________________________________________
JSON_400 = partial(JSONResponse, status_code=status.HTTP_400_BAD_REQUEST)


class Resource(NamedTuple):
    name: str  # Name of the Device
    object: Type  # Device class instance
    commands: List[str]  # List of available commands to call for the Device


class Task(BaseModel):
    """
    example = {
        'chain': True,  #<-- specifies if all devices should be locked for task
        'resources':[  #<-- Lab Resources to run commands on
            {'SDS_2304X_1':[     #<-- Lab Device w/ list of commands to run
                {'query':['*IDN?',]},  #<-- Command w/ list of command args
                {'read':['*IDN?',]},
            ]},
            {'SDS_2304X_2':[
                {'query':['*IDN?',]}
            ]},
        ]
    }
    """
    chain: bool = False
    resources: List[Dict[str, List[Dict[str, List[str]]]]]


class SerializedTask(BaseModel):
    """
    example = {
        'task_id': 'f2612c45-4e34-4ee7-a86d-16f0c836314c',
        'task': Task,  #<-- Task object above
    }
    """
    task_id: str
    task: Task


# ******************************************************************************#
#                                REDIS FUNCS                                   #
# ******************************************************************************#
# __ Global Variables ________________________________________________________
RESOURCE_LIST = "resource_list"
ACTIVE_CHAINED_TASK_LIST = "active_chained_task_list"
TASK_LIST = "task_list"
RETURN_LIST = "return_list"
KEY_TIMEOUT = 6 * 60 * 60  # 6 hr key timeout


def ROOT_PATH():
    return rejson.Path.rootPath()


def PATH(path):
    return rejson.Path(str(path))


def task_key(task_id):
    return f"{TASK_LIST}:{task_id}"


def return_key(task_id):
    return f"{RETURN_LIST}:{task_id}"


class RedisJsonCore:
    def __init__(self, host, port):
        self.host, self.port = host, port
        self.client = self.get_client(host=self.host, port=self.port)

    # __ RedisJson Client Creation _______________________________________________
    def get_client(self, host: str = '', port: int = 0) -> rejson.client.Client:
        _host = host or self.host
        _port = port or self.port
        return rejson.Client(host=_host, port=_port, decode_responses=True)

    # __ RedisJson Transaction Helpers ___________________________________________
    def transaction(self, transaction_func: Callable, watch_list: Tuple[str]) -> Any:
        return self.client.transaction(transaction_func, *watch_list, value_from_callable=True)

    # __ Key-Value Pair Initialization ___________________________________________
    def create_resource_list(self, force: bool = False) -> bool:
        if not force and self.client.exists(RESOURCE_LIST):
            return True

        return self.client.jsonset(RESOURCE_LIST, ROOT_PATH(), {})

    # __ RedisJson Transaction Functions ________________________________________
    # ____ - Task Management Transactions _______________________________________
    def task_list_add_task(self, task: SerializedTask) -> str:
        task_id = task.task_id
        task_info = {
            "busy": False,
            "chain": task.task.chain,
            "resources": task.task.resources,
        }

        def transaction_func(pipe: rejson.client.Pipeline) -> None:
            pipe.multi()
            pipe.rpush(TASK_LIST, task_id)
            pipe.jsonset(task_key(task_id), ROOT_PATH(), task_info)

        self.transaction(transaction_func, (RESOURCE_LIST))
        return task_id

    def task_list_rm_task(self, task_id: str) -> str:
        def transaction_func(pipe: rejson.client.Pipeline) -> None:
            pipe.multi()
            pipe.lrem(TASK_LIST, 1, task_id)
            pipe.jsondel(task_key(task_id))

        self.transaction(transaction_func, (RESOURCE_LIST, task_key(task_id)))
        return task_id

    def task_list_length(self) -> int:
        return self.client.llen(TASK_LIST)

    def task_list(self, start_idx: int = 0, stop_idx: int = -1) -> List:
        return self.client.lrange(TASK_LIST, start_idx, stop_idx)

    def task_list_get_tasks(self, task_id_list: List[str]):
        tasks = (task_key(task_id) for task_id in task_id_list)
        return self.client.jsonmget(ROOT_PATH(), *tasks)

    def task_list_check_task_resources(self, task_id: str, pipe: Union[rejson.client.Pipeline, None] = None) -> List:
        if pipe:
            return pipe.jsonget(task_key(task_id), PATH(".resources"))
        return self.client.jsonget(task_key(task_id), PATH(".resources"))

    def task_list_check_task_is_busy(self, task_id: str) -> bool:
        return self.client.jsonget(task_key(task_id), PATH(".busy"))

    def task_list_check_task_is_chain(self, task_id: str) -> bool:
        return self.client.jsonget(task_key(task_id), PATH(".chain"))

    def task_list_update_task_busy_field(
        self,
        task_id: str, busy: bool,
        pipe: Union[rejson.client.Pipeline, None] = None
    ) -> bool:
        if pipe:
            return pipe.jsonset(task_key(task_id), PATH(".busy"), busy)
        return self.client.jsonset(task_key(task_id), PATH(".busy"), busy)

    def task_list_pop_first_task_resource(self, task_id: str, pipe: Union[rejson.client.Pipeline, None] = None):
        if pipe:
            return pipe.jsonarrpop(task_key(task_id), PATH(".resources"), 0)
        return self.client.jsonarrpop(task_key(task_id), PATH(".resources"), 0)

    # ____ - Chained Task Management Transactions ________________________________
    def active_chained_task_list_add_task(self, task_id: str) -> bool:
        return self.client.sadd(ACTIVE_CHAINED_TASK_LIST, task_id)

    def active_chained_task_list(self) -> set:
        return self.client.smembers(ACTIVE_CHAINED_TASK_LIST)

    def active_chained_task_list_rm_task(self, task_id: str, pipe: Union[rejson.client.Pipeline, None] = None) -> bool:
        if pipe:
            return pipe.srem(ACTIVE_CHAINED_TASK_LIST, task_id)
        return self.client.srem(ACTIVE_CHAINED_TASK_LIST, task_id)

    # ____ - Completed Task Transactions _________________________________________
    def return_list_complete_task(self, task_id: str, result_timeout=KEY_TIMEOUT) -> bool:
        completed = self.client.jsonset(return_key(task_id), PATH(".complete"), True)
        self.client.expire(return_key(task_id), result_timeout)
        return completed

    def return_list_check_task_completion(self, task_id: str) -> bool:
        completed = self.client.jsonget(return_key(task_id), PATH(".complete"))
        return completed

    def return_list_add_task_resource(self, task_id: str, resource_name: str, func_names: List[str]):
        tid = return_key(task_id)
        func_set = set(func_names)

        if self.client.jsonget(tid):
            if resource_name in self.client.jsonobjkeys(tid):
                current_func_set = set(self.client.jsonobjkeys(tid, PATH(f".{resource_name}")))
                for func_name in func_set - current_func_set:
                    self.client.jsonset(tid, PATH(f".{resource_name}.{func_name}"), [])
            else:
                tid_resource_funcs = {func_name: [] for func_name in func_set}
                self.client.jsonset(tid, PATH(f".{resource_name}"), tid_resource_funcs)
        else:
            tid_resource_funcs = {func_name: [] for func_name in func_set}
            tid_resource_result_dict = {resource_name: tid_resource_funcs, "complete": False}
            self.client.jsonset(tid, ROOT_PATH(), tid_resource_result_dict)

    def return_list_add_task_resource_func_output(
        self,
        task_id: str, resource_name: str,
        func_name: str, func_return: Any
    ):
        return self.client.jsonarrappend(return_key(task_id), PATH(f".{resource_name}.{func_name}"), func_return)

    def return_list_get_full_task_output(self, task_id: str):
        return self.client.jsonget(return_key(task_id))

    # ____ - Resource Management Transactions ____________________________________
    def resource_list_add_resource(self, resource_name: str) -> bool:
        if resource_name in self.client.jsonobjkeys(RESOURCE_LIST):
            return True

        return self.client.jsonset(RESOURCE_LIST, PATH(f".{resource_name}"), "")

    def resource_list_update_resource_user(
        self,
        resource_name: str, task_id: str = "",
        pipe: Union[rejson.client.Pipeline, None] = None
    ):
        if pipe:
            return pipe.jsonset(RESOURCE_LIST, PATH(f".{resource_name}"), task_id)
        return self.client.jsonset(RESOURCE_LIST, PATH(f".{resource_name}"), task_id)

    def resource_list_get_resources(self):
        return self.client.jsonget(RESOURCE_LIST)


# ******************************************************************************#
#                             REDIS UTILITY CLASS                              #
# ******************************************************************************#


class RedisJsonClient:
    def __init__(self, host: str, port: int, force_init=False):
        self.__core = RedisJsonCore(host=host, port=port)
        self.__init_redis(force_init=force_init)
        self.host = host
        self.port = port

    def __init_redis(self, force_init=False) -> None:
        self.__core.create_resource_list(force=force_init)

    # __ TASK RELATED METHODS _______________________________________________
    def push_task(self, serialized_task: SerializedTask) -> str:
        task_id = self.__core.task_list_add_task(task=serialized_task)
        return task_id

    def task_list_length(self) -> int:
        task_list_len = self.__core.task_list_length()
        return task_list_len

    def task_is_chained(self, task_id: str) -> bool:
        return self.__core.task_list_check_task_is_chain(task_id)

    def get_task_resource_names(self, task_id: str) -> List[str]:
        resource_info = self.__core.task_list_check_task_resources(task_id=task_id)
        resource_names = [next(iter(name)) for name in resource_info]
        return resource_names

    def get_task_range(self, range_start: int = 0, range_end: int = -1) -> List[str]:
        return self.__core.task_list(start_idx=range_start, stop_idx=range_end)

    def get_tasks(self, task_id_list: List[str]):
        return self.__core.task_list_get_tasks(task_id_list=task_id_list)

    def get_task_resource(self, task_id: str):
        resources = self.__core.task_list_check_task_resources(task_id)
        return resources[0]

    def set_task_status(self, task_id: str, busy: bool, update_task: bool = True) -> None:
        if update_task:
            self.__core.task_list_update_task_busy_field(task_id, busy)
        return None

    def pop_task_resource(self, task_id: str):
        " Pops & returns the first resource dict of the task "

        def transaction_func(pipe: rejson.client.Pipeline) -> None:
            self.__core.task_list_pop_first_task_resource(task_id, pipe=pipe)
            resources = self.__core.task_list_check_task_resources(task_id, pipe=pipe)
            return len(resources)

        return self.__core.transaction(transaction_func, (RESOURCE_LIST, task_key(task_id)))

    def pop_task(self, task_id: str, task_is_chained: bool) -> str:
        task_id = self.__core.task_list_rm_task(task_id=task_id)
        if task_is_chained:

            def transaction_func(pipe: rejson.client.Pipeline) -> None:
                resources = self.__core.resource_list_get_resources()
                chained_resources = [k for k, v in resources.items() if task_id == v]
                pipe.multi()
                for resource in chained_resources:
                    self.__core.resource_list_update_resource_user(resource, "", pipe=pipe)
                self.__core.active_chained_task_list_rm_task(task_id, pipe=pipe)

            self.__core.transaction(transaction_func, (RESOURCE_LIST,))

        self.__core.return_list_complete_task(task_id=task_id)

        return task_id

    # __ CHAINED TASK RELATED METHODS ________________________________________
    def get_active_chained_tasks(self) -> set:
        return self.__core.active_chained_task_list()

    # __ TASK RETURN RELATED METHODS _________________________________________
    def create_resource_return_dict(self, task_id: str, resource_name: str, func_names: List[str]):
        self.__core.return_list_add_task_resource(task_id, resource_name, func_names)

    def store_func_return(self, task_id: str, resource_name: str, func_name: str, func_return: Any):
        self.__core.return_list_add_task_resource_func_output(task_id, resource_name, func_name, func_return)

    def get_task_result(self, task_id: str):
        if self.__core.return_list_check_task_completion(task_id):
            return self.__core.return_list_get_full_task_output(task_id)
        return None

    # __ RESOURCE RELATED METHODS ____________________________________________
    def add_resource(self, resource_name: str) -> bool:
        resource_added = self.__core.resource_list_add_resource(resource_name=resource_name)
        return resource_added

    def get_unintilized_resources(self) -> List[str]:
        resources = self.__core.resource_list_get_resources()
        unitilized_resources = [k for k, v in resources.items() if v == ""]
        return unitilized_resources

    def lock_resource(self, resource_name: str, task_id: str, task_is_chained: bool) -> None:
        def transaction_func(pipe: rejson.client.Pipeline) -> None:
            pipe.multi()
            self.__core.resource_list_update_resource_user(resource_name, task_id, pipe=pipe)
            self.__core.task_list_update_task_busy_field(task_id, True, pipe=pipe)

        self.__core.transaction(transaction_func, (RESOURCE_LIST, task_key(task_id)))

        if task_is_chained:
            self.__core.active_chained_task_list_add_task(task_id)

    def unlock_resource(self, resource_name: str, task_id: str, update_task: bool = True) -> None:
        def transaction_func(pipe: rejson.client.Pipeline) -> None:
            pipe.multi()
            self.__core.resource_list_update_resource_user(resource_name, "", pipe=pipe)
            if update_task:
                self.__core.task_list_update_task_busy_field(task_id, False, pipe=pipe)

        self.__core.transaction(transaction_func, (RESOURCE_LIST, task_key(task_id)))


# ******************************************************************************#
#                            GENERAL UTILITY FUNCS                              #
# ******************************************************************************#
ErrorContainer = namedtuple("ErrorContainer", ["error_type", "error_msg"])


class ConfigReader:
    ResourceInfo = namedtuple("ResourceInfo", ["class_name", "init_args"])

    def __init__(self, cfg_file: str):
        self.cfg_file = cfg_file
        with open(self.cfg_file) as yml_cfg:
            self.cfg = yaml.safe_load(yml_cfg)

    def get_resources(self) -> List[str]:
        resource_names = [resource_name for resource_name in self.cfg.keys()]
        return resource_names

    def get_resource_info(self, resource_name: str):
        resource_info = self.ResourceInfo(**self.cfg[resource_name])
        return resource_info


def import_resource(resource_class: str) -> Type:
    try:
        return getattr(import_module(f"app.resources.{resource_class}"), resource_class)
    except ModuleNotFoundError as err:
        return ErrorContainer(ModuleNotFoundError, err)
    except AttributeError as err:
        return ErrorContainer(AttributeError, err)


def gen_uuid4():
    return uuid.uuid4()


def str_to_num(string: str):
    if string.isdigit():
        return int(string)
    else:
        try:
            return float(string)
        except ValueError:
            return string


def chunks(list_len: int, chunk_size: int):
    for i in range(0, list_len, chunk_size):
        temp = i + chunk_size - 1
        end = temp if temp < list_len else list_len - 1
        yield (i, end)


if __name__ == "__main__":
    pass
