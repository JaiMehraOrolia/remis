# Tests the following app route:
#   - /
from app.utils.client import RemisClient
import unittest


class TestIndex(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.client = RemisClient('127.0.0.1', 8000, test_client=True)

    def test_root(self):
        response = self.client.index()
        resp_status = response.status_code
        resp_json = response.json()
        print(f"{resp_status = }")
        print(f"{resp_json = }")
        self.assertEqual(resp_status, 200, msg=f"Received a non-200 status code of {resp_status}")
        self.assertEqual(resp_json, {"server": "RemIS"}, msg=f"Received unexpected output for route '/': {resp_json} ")


if __name__ == "__main__":
    unittest.main()
