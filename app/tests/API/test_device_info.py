# Tests the following app route:
#   - /device/{dev_name}
from app.utils.client import RemisClient
from app.utils.utils import ConfigReader
import unittest


class TestDeviceInfo(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.client = RemisClient('127.0.0.1', 8000, test_client=True)
        cls.test_resource_list = ConfigReader('app/resources/test_resources.yaml').get_resources()

    def test_device_info(self):
        for device in self.test_resource_list:
            response = self.client.device_info(device)
            resp_status = response.status_code
            resp_json = response.json()

            self.assertEqual(resp_status, 200, msg=f"Received a non-200 status code of {resp_status}")
            self.assertEqual(
                resp_json['name'],
                device,
                msg=(f"Discrepancy between configured resource name ({device}) and what was passed "
                     f"into RemIS ({resp_json['name']})")
            )

    def test_device_info_fail(self):
        fake_dev = 'Non-Existing Device'
        response = self.client.device_info(fake_dev)
        resp_status = response.status_code
        resp_json = response.json()

        self.assertEqual(resp_status, 400, msg=f"Received a non-400 status code of {resp_status}")
        self.assertIn(
            'error',
            resp_json,
            msg=f"Received unexpected output for forced fail of route '/device_info/{fake_dev}': {resp_json}"
        )


if __name__ == "__main__":
    unittest.main()
