# Tests the following app route:
#   - /list
from app.utils.client import RemisClient
from app.utils.utils import ConfigReader
import unittest


class TestList(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.client = RemisClient('127.0.0.1', 8000, test_client=True)
        cls.test_resource_list = ConfigReader('app/resources/test_resources.yaml').get_resources()

    def test_device_list(self):
        response = self.client.list_devices()
        resp_status = response.status_code
        resp_json = response.json()

        self.assertEqual(resp_status, 200, msg=f"Received a non-200 status code of {resp_status}")

        if device_list := resp_json.get('device_list', None):
            self.assertEqual(
                device_list,
                self.test_resource_list,
                msg=(f"Discrepancy between resources specified in config ({self.test_resource_list}) "
                     f"and resources returned by RemIS ({device_list})")
            )
        elif msg := resp_json.get('msg', None):
            self.assertEqual(msg, 'No resources found')
        else:
            self.fail(f"Received unexpected output for route '/list': {resp_json}")


if __name__ == "__main__":
    unittest.main()
