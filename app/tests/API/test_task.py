# Tests the following app routes for a single test:
#   - /run_task
#   - /get_task_results/{task_id}
import unittest
import time
from app.utils.client import RemisClient
from app.utils.utils import ConfigReader


class TestTask(unittest.TestCase):
    good_task_info = {
        'TestResource1': {
            'query': ('*IDN?', ),
            'write': ('arg1', )
        }
    }
    bad_task_info = {
        'TestResource1': {
            'query': ('*IDN?', ),
            'bad_read': ('', ),
            'write': ('arg1', )
        }
    }
    task_error_expectations = {'query': False, 'write': False, 'read': False, 'bad_read': True}

    @classmethod
    def setUpClass(cls):
        cls.client = RemisClient('127.0.0.1', 8000, test_client=True)
        cls.test_resource_list = ConfigReader('app/resources/test_resources.yaml').get_resources()

    @classmethod
    def __create_task(cls, task_info):
        task = cls.client.create_task()
        for resource_name, functions in task_info.items():
            resource = task.add_resource(resource_name)
            for function_name, function_args in functions.items():
                resource.add_func(function_name, *function_args)
        return task

    def __check_task_return(self, original_task_info, returned_task_info, expected_errors):
        for task_info, task_return in zip(original_task_info, returned_task_info):
            original_task_resource, task_info = task_info
            returned_task_resource, task_return = task_return
            self.assertEqual(original_task_resource, returned_task_resource, msg=f"Task resource discrepancy between original task ({original_task_resource}) & returned task ({returned_task_resource})")

            for original_func, returned_func in zip(task_info.items(), task_return.items()):
                original_func_name, _ = original_func
                returned_func_name, returned_func_results = returned_func
                self.assertEqual(original_func_name, returned_func_name, msg=f"Task resource function discrepancy between original task ({original_func_name}) & returned task ({returned_func_name})")
                error_expected = expected_errors[returned_func_name]
                for func_result in returned_func_results:
                    func_error = func_result['error']
                    self.assertEqual(error_expected, func_error, msg=f"Resource func {returned_func_name} error expected to be {error_expected} but returned {func_error}")

    # def test_good_task(self):
    #     if not set(self.good_task_info).issubset(set(self.test_resource_list)):
    #         self.fail('Cannot test /run_task or /get_task_results as there is no TestResource to run a task on')
    #     task = self.__create_task(self.good_task_info)

    #     with self.subTest('Test /run_task'):
    #         response = task.send_task()
    #         resp_status = response.status_code
    #         resp_json = response.json()

    #         self.assertEqual(resp_status, 200, msg=f"Received a non-200 status code of {resp_status}: {resp_json}")
    #         self.assertIn('task_id', resp_json, msg=f"Received unexpected output for route '/run_task': {resp_json}")

    #     with self.subTest('Test /get_task_results/{task_id}'):
    #         test_start_time, test_timeout = time.monotonic(), 60
    #         response = None
    #         while time.monotonic() < (test_start_time + test_timeout):
    #             response = task.get_result()
    #             resp_status = response.status_code
    #             resp_json = response.json()
    #             self.assertEqual(resp_status, 200, msg=f"Received a non-200 status code of {resp_status}")
    #             if resp_json['status'] != 'Done':
    #                 time.sleep(1)
    #                 continue
    #             break
    #         else:
    #             self.fail(f"Test timeout of {test_timeout}s was exceeded, task status was \'{resp_json['status']}\'. RemIS service took too long")

    #     print(resp_json['result'])
    #     self.__check_task_return(self.good_task_info.items(), resp_json['result'].items(), self.task_error_expectations)

    def test_bad_task(self):
        if not set(self.bad_task_info).issubset(set(self.test_resource_list)):
            self.fail('Cannot test /run_task or /get_task_results as there is no TestResource to run a task on')
        task = self.__create_task(self.bad_task_info)

        with self.subTest('Test /run_task'):
            response = task.send_task()
            resp_status = response.status_code
            resp_json = response.json()
            print(response)

            self.assertEqual(resp_status, 200, msg=f"Received a non-200 status code of {resp_status}: {resp_json}")
            self.assertIn('task_id', resp_json, msg=f"Received unexpected output for route '/run_task': {resp_json}")

        with self.subTest('Test /get_task_results/{task_id}'):
            test_start_time, test_timeout = time.monotonic(), 60
            response = None
            while time.monotonic() < (test_start_time + test_timeout):
                response = task.get_result()
                resp_status = response.status_code
                resp_json = response.json()
                self.assertEqual(resp_status, 200, msg=f"Received a non-200 status code of {resp_status}")
                if resp_json['status'] != 'Done':
                    time.sleep(1)
                    continue
                break
            else:
                self.fail(f"Test timeout of {test_timeout}s was exceeded, task status was \'{resp_json['status']}\'. RemIS service took too long")

        print(resp_json['result'])
        self.__check_task_return(self.bad_task_info.items(), resp_json['result'].items(), self.task_error_expectations)


if __name__ == "__main__":
    unittest.main()
