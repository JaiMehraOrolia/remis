# This test will check that the task (chained & not chained) is carried through the application correctly.
# This means that, for a single running task, the steps of execution are:
#   1.) Put in the task_list in redis
#   2.) Kicked off by RemIS if the first resource in the task is not being used (which it shouldn't if there is only
#       a single running test). The Resource is then locked
#   3.) Run in Celery a function at a time for the resource (putting function result in redis function return_list)
#       until all functions are executed
#   4.) Resource is removed from task and process repeats from step 2 unless this was the last resource.
#       The Resource is unlocked unless the task is chained.
#       The task is set to complete in the respective return_list task if this was the last resource.
#   5.) Task is removed from the task list (if it was a chained task then all resources are unlocked).
