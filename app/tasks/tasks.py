import os
from app.tasks.celery import app
from celery.utils.log import get_task_logger
from typing import Callable
from app.utils.utils import (
    RedisJsonClient,
    import_resource,
    ConfigReader,
    str_to_num
)

logger = get_task_logger(__name__)
redis_client = RedisJsonClient("rejson", 6379)
resource_file = os.getenv('REMIS_RESOURCE_CFG', 'app/resources/test_resources.yaml')

@app.task
def task_run(task_id: str, resource_name: str, task_is_chained: bool):
    # __ Initialization ______________________________________________________
    # Gather Task Resource information
    resource_info = redis_client.get_task_resource(task_id)
    func_info = next(iter(resource_info.values()))
    func_list = [next(iter(func)) for func in func_info]
    # Create storage for the task function outputs
    redis_client.create_resource_return_dict(task_id, resource_name, func_list)
    # Get Resource Class Info & Initialize Resource Class
    cfg = ConfigReader(resource_file).get_resource_info(resource_name)
    resource_class_obj = import_resource(cfg.class_name)(*cfg.init_args)
    # Create Resource in task output ledger

    # __ Function Execution Loop _____________________________________________
    for func in func_info:
        # Grab function to run & check if Resource has context manager
        func_name, arg_list = next(iter(func.items()))
        func = getattr(resource_class_obj, func_name)
        ctxt = getattr(resource_class_obj, "__enter__", None)
        logger.info(f"Running '{func_name}' with the args {arg_list} on resource '{resource_name}'")
        try:
            # Check if there's a context manager declared in the Resource class
            if ctxt:
                with resource_class_obj as _:
                    func_return = func(*map(str_to_num, arg_list))
            else:
                func_return = func(*map(str_to_num, arg_list))
            return_obj = {"message": func_return, "error": False}
        except Exception as err:
            logger.exception(f"Error in '{func_name}': {repr(err)}")
            func_return = repr(err)
            return_obj = {"message": func_return, "error": True}
        finally:
            logger.info(f"[{task_id}][{resource_name}]Storing '{func_name}' output: {return_obj}")
            redis_client.store_func_return(
                task_id, resource_name, func_name, return_obj
            )

    # __ Cleanup _____________________________________________________________
    # Remove resource requirement from task
    task_resource_amt = redis_client.pop_task_resource(task_id)
    task_not_complete = task_resource_amt > 0
    # If no more resource requirements in task, remove it from the Task List
    if not task_not_complete:
        logger.info("Task is complete, removing it from Task List")
        redis_client.pop_task(task_id, task_is_chained)

    if not task_is_chained:
        # Unlock resource used in task & set task to not busy
        redis_client.unlock_resource(resource_name, task_id, task_not_complete)
    else:
        # Do not unlock resource b/c task is chained, just set task to not busy
        redis_client.set_task_status(task_id, False, task_not_complete)

    return None
