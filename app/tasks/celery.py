from celery import Celery

REDIS_DB = 5
app = Celery('tasks',
             broker=f'redis://rejson:6379/{REDIS_DB}',
             include=['app.tasks.tasks'])
