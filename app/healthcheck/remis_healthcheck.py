import requests


def healthcheck():
    # Uses the docker host port for RemIS (which is 80)
    assert requests.get("http://localhost:80/").status_code == 200


if __name__ == "__main__":
    healthcheck()
