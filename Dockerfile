FROM python:3.8-slim-buster

# Declare all mandatory app args
ARG REMIS_RESOURCE_CONFIG

# Grab env vars
ENV REMIS_RESOURCE_CONFIG=$REMIS_RESOURCE_CONFIG

# App point of entry:
EXPOSE 80

# Install dependencies:
COPY requirements.txt .
RUN pip install -r requirements.txt

# Copy application:
COPY app/ app/

# Run application:
CMD ["uvicorn", "app.main:app", "--host", "0.0.0.0", "--port", "80" ]
