First install a virtualenv for the python dependencies, I am using python's virtualenv module however you can use the Linux virtualenv module if you'd like:
  `python3 -m pip install virtualenv`

Next create the virtualenv:
  `python3 -m virtualenv .venv`
This creates a virtualenv in the .venv directory. If you type `ls -a` then you should see `.venv/`

Now activate the virtualenv and install the python dependencies:
  `. .venv/bin/activate`
  `python3 -m pip install -r requirements.txt`
The environment and dependncies are installed now


Run the service via the following:
  `uvicorn app.main:app --reload`

If using docker
  `docker run --name RemIS -p 8000:80 remis`

ALSO **************************TEMP**************************
Dockerize celery plz, meanwhile run:
  >> celery -A app.tasks.tasks worker --autoscale=3,1 --loglevel=info

Also redis:
  >> docker run -p 6379:6379 --name rejson redislabs/rejson:latest



[NEW] ***********************TEMP***************************
Added docker-compose file so everything is launched at once.
If launching for first time when pulling repo, or when implementing changes in
code, run:
  >> docker-compose up --build

if starting up service again after images were built, run:
  >> docker-compose up